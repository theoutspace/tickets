﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tickets.Models;

namespace tickets.Controllers
{
    public class MatchesController : Controller
    {
        private ticketsEntities db = new ticketsEntities();

        // GET: Matches
        public ActionResult Index()
        {
            var match = db.Match.Include(m => m.Team).Include(m => m.Team1);
            return View(match.ToList());
        }

        public ActionResult getFiveMathes()
        {
            List<Match> Mathes = db.Match.OrderBy(a => a.date).Take(5).ToList();
            return View(Mathes);
        }

        public ActionResult getMatchById(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var match = db.Match.Find(id);
            return View(match);
        }
        

        // GET: Matches/Create
        public ActionResult Create()
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            ViewBag.firts_team = new SelectList(db.Team, "id", "name");
            ViewBag.second_team = new SelectList(db.Team, "id", "name");
            return View();
        }

        // POST: Matches/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,date,available,firts_team,second_team")] Match match)
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                
                for (int i = 0; i < 16; i++)
                {
                    Ticket ticket = new Ticket();
                    ticket.match = match.id;
                    ticket.Match1 = match;
                    //A sector;
                    ticket.sector = 1;
                    ticket.place = i;
                    db.Ticket.Add(ticket);
                    
                }

                
                for (int i = 0; i < 16; i++)
                {
                    Ticket ticket = new Ticket();
                    ticket.match = match.id;
                    ticket.Match1 = match;
                    //B sector;
                    ticket.sector = 2;
                    ticket.place = i;
                    db.Ticket.Add(ticket);
                    
                }

                
                for (int i = 0; i < 16; i++)
                {
                    Ticket ticket = new Ticket();
                    ticket.match = match.id;
                    ticket.Match1 = match;
                    //C sector;
                    ticket.sector = 3;
                    ticket.place = i;
                    db.Ticket.Add(ticket);
                   
                }

                
                for (int i = 0; i < 16; i++)
                {
                    Ticket ticket = new Ticket();
                    ticket.match = match.id;
                    ticket.Match1 = match;
                    //D sector;
                    ticket.sector = 4;
                    ticket.place = i;
                    db.Ticket.Add(ticket);
                    
                }


                db.Match.Add(match);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.firts_team = new SelectList(db.Team, "id", "name", match.firts_team);
            ViewBag.second_team = new SelectList(db.Team, "id", "name", match.second_team);
            return View(match);
        }

        // GET: Matches/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = db.Match.Find(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            ViewBag.firts_team = new SelectList(db.Team, "id", "name", match.firts_team);
            ViewBag.second_team = new SelectList(db.Team, "id", "name", match.second_team);
            return View(match);
        }

        // POST: Matches/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,date,available,firts_team,second_team")] Match match)
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(match).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.firts_team = new SelectList(db.Team, "id", "name", match.firts_team);
            ViewBag.second_team = new SelectList(db.Team, "id", "name", match.second_team);
            return View(match);
        }

        // GET: Matches/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = db.Match.Find(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            return View(match);
        }

        // POST: Matches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsInRole("Admin"))
            {
                return HttpNotFound();
            }
            Match match = db.Match.Find(id);
                    
            db.Match.Remove(match);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
