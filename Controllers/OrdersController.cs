﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tickets.Models;

namespace tickets.Controllers
{
    public class OrdersController : Controller
    {
        private ticketsEntities db = new ticketsEntities();
       
        public ActionResult MyOrders()
        {
            List<Order> MyOrder = new List<Order>();
            string userId=User.Identity.GetUserId();
            if (User.Identity.IsAuthenticated)
            {
                MyOrder = db.Order.Where(t => t.id_User == userId).OrderByDescending(t => t.date).ToList();
            }
           
            return View(MyOrder);
        }

        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Ticket> Tickets = new List<Ticket>();
            if (User.Identity.IsAuthenticated)
            {
                List<Ticket> TicketsA = db.Ticket.Where(t => t.match == id).ToList();
                
                return View(TicketsA);
            }
            return View(Tickets);
        }


        [Serializable]
        public class MItemInputModel
        {            
            public string[] Params { get; set; }
        }

        [HttpPost]
        public JsonResult PreOrder(MItemInputModel inputModel)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Json("Error");
            }
            List < String > stringId= new List<string>();
            List<int> orderId = new List<int>();
            stringId = inputModel.Params.ToList();
            foreach(var item in stringId)
            {
                int number;
                string resp = (Int32.TryParse(item, out number)).ToString();
                if (number > 0)
                {
                    orderId.Add(number);
                }
            }
            
            List<Ticket> PreOrderTickets = new List<Ticket>();
            foreach(var item in orderId)
            {
                Ticket ticket = db.Ticket.Find(item);
                if(ticket==null)
                {
                    return Json("Error");
                }
                if(ticket.state!=0)
                {
                    return Json("Билет уже заказан");
                }
                PreOrderTickets.Add(ticket);
            }

            foreach(var item in PreOrderTickets)
            {
                Order order = new Order();
                item.state = 2;
                db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                User.Identity.GetUserId();
                order.date = DateTime.Now;
                order.id_Ticket = item.id;
                order.id_User = User.Identity.GetUserId();
                db.Order.Add(order);
            }

            db.SaveChanges();
            return Json(new
            {
                redirectUrl = Url.Action("MyOrders", "Orders"),
                isRedirect = true
            });

        }
        

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = db.Order.Find(id);
            if (order.id_User != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<Ticket> ticket = db.Ticket.Where(t => t.id == order.id_Ticket).ToList();
            if (ticket.Count() == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket myticket = ticket.Last();
            
            return View(myticket);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Order.Find(id);
            
            Ticket ticket = db.Ticket.Find(order.id_Ticket);
            ticket.state = 0;
            db.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
            db.Order.Remove(order);
            db.SaveChanges();
            return RedirectToAction("MyOrders");
        }

        public ActionResult Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Ticket.Find(id);
            List<Order> order = db.Order.Where(t => t.id_Ticket == id).ToList();
            if (order.Count() == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order MyOrder = order.Last();
            if (MyOrder.id_User != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(ticket);
        }


        public ActionResult Confirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!User.Identity.IsAuthenticated)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Ticket.Find(id);
            List<Order> order = db.Order.Where(t => t.id_Ticket == id).ToList();
                
            foreach (var user in order) { 
                if (user.id_User != User.Identity.GetUserId())
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }                
            }
            if (ticket.state == 3)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ticket.state = 3;
            db.Entry(ticket).State = System.Data.Entity.EntityState.Modified;

            db.SaveChanges();

            return RedirectToAction("MyOrders");

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
